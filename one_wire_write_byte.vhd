----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:57:02 04/26/2016 
-- Design Name: 
-- Module Name:    one_wire_write_byte - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity one_wire_write_byte is
    Port ( DI : in  STD_LOGIC_VECTOR (7 downto 0);
			  rst : in STD_LOGIC;
           clk : in  STD_LOGIC;
           bitWriterEnabled : in  STD_LOGIC;
			  enableWriting : in STD_LOGIC;
           busy : out  STD_LOGIC;
           output0 : out  STD_LOGIC;
			  output1 : out  STD_LOGIC);
end one_wire_write_byte;

architecture Behavioral of one_wire_write_byte is
	type state_type is (IDLE, READ_BYTE, WAIT_FOR_WRITER, WRITE_BIT, INCREMENT);
	signal state : state_type := idle;
	signal next_state : state_type := idle;
	signal enableWritingDummy : STD_LOGIC;
	signal write_counter : integer := 0;
	signal DIDummy : STD_LOGIC_VECTOR(7 downto 0) := "00000000";
begin

	main_process : process(clk)
	begin
		if rising_edge(clk) then
			state <= next_state;
		end if;
	end process;

	output_state_process : process(clk, state)
	begin
	
		if rising_edge(clk) then
			output0 <= '0';
			output1 <= '0';
			case state is
					when IDLE =>
						write_counter <= 0;
						busy <= '0';
						enableWritingDummy <= enableWriting;
						
					when READ_BYTE =>
						DIDummy <= DI;
						busy <= '1';
						
					when WRITE_BIT =>
						if DIDummy(write_counter) = '0' then
							output0 <= '1';
						else
							output1 <= '1';
						end if;
					
					when INCREMENT =>
						write_counter <= write_counter + 1;
					
					when others =>
						null;
			end case;
		end if;
	end process;
	
	state_switcher : process(clk, state, write_counter, bitWriterEnabled, enableWritingDummy)
	begin
		next_state <= state;
		
		case state is
			when IDLE =>
				if bitWriterEnabled = '1' AND enableWritingDummy = '1' then
					next_state <= READ_BYTE;
				end if;
				
			when READ_BYTE =>
				next_state <= WRITE_BIT;
				
			when WAIT_FOR_WRITER =>
				if bitWriterEnabled = '1' then
					if write_counter = 8 then
						next_state <= IDLE;
					else
						next_state <= WRITE_BIT;
					end if;
				end if;
						
			when WRITE_BIT =>
				next_state <= INCREMENT;
				
			when INCREMENT =>
				next_state <= WAIT_FOR_WRITER;
		end case;
	end process;
end Behavioral;

