--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:59:12 04/08/2016
-- Design Name:   
-- Module Name:   C:/Users/lab/Desktop/ONE_WIRE/robsdrops-one-wire-cf2d34790817/turbo2bencz.vhd
-- Project Name:  OneWire
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: one_wire_bit
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY turbo2bencz IS
END turbo2bencz;
 
ARCHITECTURE behavior OF turbo2bencz IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT one_wire_bit
    PORT(
         wr1 : IN  std_logic;
         wr0 : IN  std_logic;
         rd : IN  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
         busy : OUT  std_logic;
         output : OUT  std_logic;
         busOut : OUT  std_logic;
         busIn : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal wr1 : std_logic := '0';
   signal wr0 : std_logic := '0';
   signal rd : std_logic := '0';
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal busIn : std_logic := '0';

 	--Outputs
   signal busy : std_logic;
   signal output : std_logic;
   signal busOut : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: one_wire_bit PORT MAP (
          wr1 => wr1,
          wr0 => wr0,
          rd => rd,
          rst => rst,
          clk => clk,
          busy => busy,
          output => output,
          busOut => busOut,
          busIn => busIn
        );

   -- Clock process definitions

	clk <= not clk after clk_period / 2; -- wait for clk_period/2; clk <= '1'; wait for clk_period/2;
 

   -- Stimulus process
   stim_proc: process
   begin		
		rst <= '1';
      wait for 1000 us;	
		rd <= '1';
      wait for 1000 us;	
		wr1 <= '1';
      wait;
   end process;

END;
