----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    12:23:52 04/02/2016
-- Design Name:
-- Module Name:    one_wire_bit - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity one_wire_bit is
    Port ( wr1 : in  STD_LOGIC;
           wr0 : in  STD_LOGIC;
           rd : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           busy : out  STD_LOGIC;
			  bit_output : out STD_LOGIC;
           presence_indicator : out  STD_LOGIC;
           busOut : out  STD_LOGIC;
           busIn : in  STD_LOGIC);
end one_wire_bit;

architecture Behavioral of one_wire_bit is
	type state_type is (IDLE, RESET, RECEIVE, PRESENCE, SET_PRESENCE, SET_NONE, WR0_0, WR0_1, WR1_0, WR1_1, RD_INIT, RD_SAMPLE, RD_1, RD_2);
	signal state, next_state : state_type;
	signal busInDummy : STD_LOGIC;
	signal cnt : STD_LOGIC_VECTOR(0 to 15) := "0000000000000000";
begin
	counter : process(clk, state)
	begin
		if rising_edge(clk) then
			case state is
				when IDLE =>
					cnt <= "0000000000000000";
				when others =>
					cnt <= cnt + 1;
			end case;
		end if;
	end process counter;

	main_process : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				state <= RESET;
			else
				state <= next_state;
			end if;
		end if;
	end process;

	output_state_process : process(clk, state, busIn, cnt, busInDummy)
	begin
		if rising_edge(clk) then
			case state is
				when IDLE =>
					busy <= '0';
					busOut <= '1';
					busInDummy <= '1';
				when RESET =>
					busy <= '1';
					busOut <= '0';
					if cnt = "0101110111000000" then -- 480 * 50
						busOut <= '1';
					end if;
				when RECEIVE =>
					if cnt = "0110101101101100" then -- 550 * 50
						busInDummy <= busIn;
					end if;
				when PRESENCE =>
				when SET_PRESENCE =>
					presence_indicator <= '1';
					busOut <= '1';
				when SET_NONE =>
					presence_indicator <= '0';
				when WR0_0 =>
					busy <= '1';
					busOut <= '0';
				when WR0_1 =>
					busOut <= '1';
				when WR1_0 =>
					busy <= '1';
					busOut <= '0';
				when WR1_1 =>
					busOut <= '1';
				when RD_INIT =>
					busOut <= '0';
				when RD_SAMPLE =>
					if cnt = "0000001011101110" then -- 15 * 50	
						busy <= '1';					
						bit_output <= busIn;
					end if;
				when RD_1 =>
					if cnt = "0000101110111000"then -- 60 * 50
						busOut <= '1';
					end if;
				when RD_2 =>
			end case;
		end if;
	end process;
	
	state_switch : process(state, wr1, wr0, rd, cnt, busInDummy)
	begin
		next_state <= state;

		case state is
			when IDLE =>
				if wr1 = '1' then
					next_state <= WR1_0;
				elsif wr0 = '1' then
					next_state <= WR0_0;
				elsif rd = '1' then
					next_state <= RD_INIT;
				end if;
			when RESET =>
				if cnt = "0101110111000000" then -- 480 * 50
					next_state <= RECEIVE;
				end if;
			when RECEIVE =>
				if cnt = "0110101101101100" then -- 550 * 50
					next_state <= PRESENCE;
				end if;
			when PRESENCE =>
				if cnt = "1011101110000000" then -- 960 * 50
					if busInDummy = '0' then
						next_state <= SET_PRESENCE;
					else
						next_state <= SET_NONE;
					end if;
				end if;
			when SET_PRESENCE =>
				next_state <= IDLE;
			when SET_NONE =>
				next_state <= IDLE;
			when WR1_0 =>
				if cnt = "0000000100101100" then -- 6 * 50
					next_state <= WR1_1;
				end if;
			when WR1_1 =>
				if cnt = "0000110110101100" then -- 70 * 50
					next_state <= IDLE;
				end if;
			when WR0_0 =>
				if cnt = "0000101110111000" then -- 60 * 50
					next_state <= WR0_1;
				end if;
			when WR0_1 =>
				if cnt = "0000110110101100" then -- 70 * 50
					next_state <= IDLE;
				end if;
			when RD_INIT =>
				if cnt = "0000000100101100" then -- 6 * 50
					next_state <= RD_SAMPLE;
				end if;
			when RD_SAMPLE =>
				if cnt = "0000001011101110" then -- 15 * 50
					next_state <= RD_1;
				end if;
			when RD_1 =>
				if cnt = "0000101110111000"then -- 60 * 50
					next_state <= RD_2;
				end if;
			when RD_2 =>
				if cnt = "0000110110101100" then -- 70 * 50
					next_state <= IDLE;
				end if;
		end case;
	end process state_switch;
end Behavioral;