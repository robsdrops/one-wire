----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:31:09 05/17/2016 
-- Design Name: 
-- Module Name:    command_module - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity command_module is
    Port ( startWrite : out  STD_LOGIC;
           startRead : out  STD_LOGIC;
			  bit_module_busy : in STD_LOGIC;
			  start : in STD_LOGIC;
           temperatureData : in  STD_LOGIC_VECTOR (23 downto 0);
           outputData : out  STD_LOGIC_VECTOR (7 downto 0);
			  clk : in STD_LOGIC;
           busy : out  STD_LOGIC;
           inputData : in  STD_LOGIC_VECTOR (7 downto 0));
end command_module;

architecture Behavioral of command_module is
	type state_type is (IDLE,
	WAIT_FOR_BUSY_RESET_1,
	WAIT_FOR_BUSY_RESET_2,
	WAIT_FOR_BUSY_RESET_3,
	RESET_1,
	RESET_2,
	RESET_3,
	WAIT_FOR_RESET_1,
	WAIT_FOR_RESET_2,
	WAIT_FOR_RESET_3,
	WAIT_FOR_BUSY_SKIP_ROM_1,
	WAIT_FOR_BUSY_SKIP_ROM_2,
	SKIP_ROM_1,
	SKIP_ROM_2,
	WAIT_FOR_SKIP_ROM_1,
	WAIT_FOR_SKIP_ROM_2,
	WAIT_FOR_BUSY_CONVERT_T,
	CONVERT_T, 
	WAIT_FOR_CONVERT_T,
	WAIT_FOR_BUSY_END_OF_CONVERT,
	READ_BYTE_CONVERT_T,
	WAIT_FOR_READ_BYTE_CONVERT_T,
	CONVERT_T_END_OR_NOT,
	WAIT_FOR_BUSY_READ_SCRATCHPAD,
	READ_SCRATCHPAD,
	WAIT_FOR_BUSY_READ_BYTE_SCRATCHPAD,
	READ_BYTE_SCRATCHPAD,
	WAIT_FOR_READ_BYTE_SCRATCHPAD,
	INCREMENT,
	WAIT_FOR_READ_SCRATCHPAD,
	CAPTURE_BYTE_END_OR_NOT,
	FINISH
	);
	signal state : state_type := IDLE ;
	signal next_state : state_type := IDLE ;
	
	signal temperatureDataDummy : STD_LOGIC_VECTOR (23 downto 0) := X"000000";
	signal read_counter : integer  range 0 to 10 := 0;
	
	begin
	clock: process (clk) is
		begin
			if rising_edge(clk) then
				state <= next_state;
			end if;
	end process;

	state_process : process(clk, state, inputData, bit_module_busy, read_counter)
	begin
		next_state <= state;
		case state is
			when IDLE =>
				if start = '1' then
					next_state <= WAIT_FOR_BUSY_RESET_1;
				end if;
			when WAIT_FOR_BUSY_RESET_1 =>
				if bit_module_busy = '0' then
					next_state <= RESET_1;
				end if;
			when RESET_1 =>
					if bit_module_busy = '1' then
						next_state <= WAIT_FOR_RESET_1;
					end if;
			when WAIT_FOR_RESET_1 =>
				if bit_module_busy = '0' then
						next_state <= WAIT_FOR_BUSY_SKIP_ROM_1;
				end if;
			when WAIT_FOR_BUSY_SKIP_ROM_1 =>
				if bit_module_busy = '0' then
						next_state <= SKIP_ROM_1;
				end if;
			when SKIP_ROM_1 =>
				if bit_module_busy = '1' then
					next_state <= WAIT_FOR_SKIP_ROM_1;
				end if;
			when WAIT_FOR_SKIP_ROM_1 =>
				if bit_module_busy = '0' then
					next_state <= CONVERT_T;
				end if;
			when WAIT_FOR_BUSY_CONVERT_T =>
				if isBusy = '0' then
						next_state <= CONVERT_T;
				end if;
			when CONVERT_T =>
					next_state <= WAIT_FOR_CONVERT_T;
			when WAIT_FOR_CONVERT_T =>
				if bit_module_busy = '0' then
					next_state <= WAIT_FOR_BUSY_END_OF_CONVERT;
				end if;
			when WAIT_FOR_BUSY_END_OF_CONVERT =>
				if bit_module_busy = '0' then
					next_state <= READ_BYTE_CONVERT_T;
				end if;
			when READ_BYTE_CONVERT_T =>
				if bit_module_busy = '0' then
					next_state <= WAIT_FOR_READ_BYTE_CONVERT_T;
				end if;
			when WAIT_FOR_READ_BYTE_CONVERT_T =>
				if bit_module_busy = '0' then
					next_state <= CONVERT_T_END_OR_NOT;
				end if;
			when CONVERT_T_END_OR_NOT =>
				if inputData = X"FF" then
					next_state <= WAIT_FOR_BUSY_RESET_2;
				else
					next_state <= WAIT_FOR_BUSY_END_OF_CONVERT;
				end if;
			when WAIT_FOR_BUSY_RESET_2 =>
				if bit_module_busy = '0' then 
					next_state <= RESET_2;
				end if;
			when RESET_2 =>
				if bit_module_busy = '1' then 
					next_state <= WAIT_FOR_RESET_2;
				end if;
			when WAIT_FOR_RESET_2 =>
				if bit_module_busy = '0' then
						next_state <= WAIT_FOR_BUSY_SKIP_ROM_2;
				end if;
			when WAIT_FOR_BUSY_SKIP_ROM_2 => 
				if bit_module_busy = '0' then
					next_state <= SKIP_ROM_2;
				end if;
			when SKIP_ROM_2 =>
				if bit_module_busy = '1' then
					next_state <= WAIT_FOR_SKIP_ROM_2;
				end if;
			when WAIT_FOR_SKIP_ROM_2 =>
				if bit_module_busy = '1' then
					next_state <= WAIT_FOR_BUSY_READ_BYTE_SCRATCHPAD;
				end if;
			when WAIT_FOR_BUSY_READ_BYTE_SCRATCHPAD =>
				if bit_module_busy = '0' then 
					next_state <= READ_SCRATCHPAD;
				end if;
			when READ_SCRATCHPAD =>
				if bit_module_busy = '1' then
					next_state <= WAIT_FOR_BUSY_READ_BYTE_SCRATCHPAD;
				end if;
			when WAIT_FOR_BUSY_READ_BYTE_SCRATCHPAD =>
				if bit_module_busy = '0' then
					next_state <= READ_BYTE_SCRATCHPAD;
				end if;
			when READ_BYTE_SCRATCHPAD =>
				if bit_module_busy = '0' then
					next_state <= WAIT_FOR_READ_BYTE_SCRATCHPAD;
				end if;
			when WAIT_FOR_READ_BYTE_SCRATCHPAD =>
				if bit_module_busy ='0' then
					next_state <= INCREMENT;
				end if;
			when INCREMENT =>
				next_state <= WAIT_FOR_READ_SCRATCHPAD;	
			when WAIT_FOR_READ_SCRATCHPAD =>
				if bit_module_busy = '0' then
					next_state <= CAPTURE_BYTE_END_OR_NOT;
				end if;
			when CAPTURE_BYTE_END_OR_NOT =>
				if read_counter = 3 then
					next_state <= WAIT_FOR_BUSY_RESET_3;
				else
					next_state <= WAIT_FOR_BUSY_READ_BYTE_SCRATCHPAD;
				end if;
			when WAIT_FOR_BUSY_RESET_3 =>
				if bit_module_busy = '0' then 
					next_state <= RESET_3;
				end if;
			when RESET_3 =>
				if bit_module_busy = '1' then
						next_state <= WAIT_FOR_RESET_3;
				end if;
			when WAIT_FOR_RESET_3 => 
				if bit_module_busy = '0' then
						next_state <= FINISH;
				end if;
			when FINISH =>
				next_state <= IDLE;
			when others =>
					next_state <= IDLE;
		end case;
	end process;
	
	action_process : process(clk, state)
	begin
		if rising_edge(clk) then
		startRead <= '0';
		startWrite <= '0';
		startReset <= '0';
		case state is
			when RESET_1 =>
				startReset <= '1';
			when RESET_2 =>
				startReset <= '1';
			when RESET_3 =>
				startReset <= '1';
			when SKIP_ROM_1 =>
				busy <= '1';
				outputData <= X"CC";
				startWrite <= '1';
			when CONVERT_T =>
				outputData <= X"44";
				startWrite <= '1';
			when READ_BYTE_CONVERT_T =>
				startRead <= '1';
			when READ_BYTE_SCRATCHPAD =>
				startRead <= '1';
			when READ_SCETCHPAD =>
				outputData <= X"BE";
				startWrite <= '1';
			when INCREMENT =>
				read_counter <= read_counter +1;
			when CAPTURE_BYTE_END_OR_NOT =>
				startRead <= '0';
				temperatureDataDummy ( (read_counter * 8 - 1) downto ( (read_counter -1) * 8 )) <= inputData;
			when IDLE =>
				read_counter <= 0;
			when others =>
				startRead <= '0';
				startWrite <= '0';
				startReset <= '0';
			end case;
		end if;
	end process;
	
	process (clk) is
	begin
		if rising_edge(clk) and state = FINISH then
			temperatureData <= temperatureDataDummy;
		end if;
	end process;
end Behavioral;
