--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:35:52 05/16/2016
-- Design Name:   
-- Module Name:   /home/robert/Documents/University of Technology/one-wire/twb_RdByte.vhd
-- Project Name:  OneWire
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: one_wire_read_byte
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY twb_RdByte IS
END twb_RdByte;
 
ARCHITECTURE behavior OF twb_RdByte IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT one_wire_read_byte
    PORT(
         rst : IN  std_logic;
         clk : IN  std_logic;
         bitReaderEnabled : IN  std_logic;
         enabledReading : IN  std_logic;
         rd_bit_input : IN  std_logic;
         busy : OUT  std_logic;
         DI : OUT  std_logic_vector(7 downto 0);
         rd_bit_command : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal bitReaderEnabled : std_logic := '1';
   signal enabledReading : std_logic := '0';
   signal rd_bit_input : std_logic := '0';

 	--Outputs
   signal busy : std_logic;
   signal DI : std_logic_vector(7 downto 0);
   signal rd_bit_command : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: one_wire_read_byte PORT MAP (
          rst => rst,
          clk => clk,
          bitReaderEnabled => bitReaderEnabled,
          enabledReading => enabledReading,
          rd_bit_input => rd_bit_input,
          busy => busy,
          DI => DI,
          rd_bit_command => rd_bit_command
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 150 ns;	
		
		rst <= '1', '0' after clk_period;
		enabledReading <= '1', '0' after clk_period * 2;
      wait;
   end process;
	
	process
	begin
		loop
			wait until rising_edge( clk ) and ( rd_bit_command = '1' );
			bitReaderEnabled <= '0';
			rd_bit_input <= '1';
			wait for clk_period * 2;
			bitReaderEnabled <= '1';
			rd_bit_input <= '0';
		end loop;
   end process;
END;
