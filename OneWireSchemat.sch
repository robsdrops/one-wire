<?xml version="1.0" encoding="UTF-8"?>
<drawing version="7">
    <attr value="spartan3e" name="DeviceFamilyName">
        <trait delete="all:0" />
        <trait editname="all:0" />
        <trait edittrait="all:0" />
    </attr>
    <netlist>
        <signal name="ROT_A" />
        <signal name="ROT_B" />
        <signal name="XLXN_20" />
        <signal name="XLXN_25" />
        <signal name="busy_led" />
        <signal name="OW_DQ" />
        <signal name="XLXN_33" />
        <signal name="XLXN_43" />
        <signal name="XLXN_44" />
        <signal name="output_led" />
        <signal name="XLXN_60" />
        <signal name="XLXN_61" />
        <signal name="XLXN_62" />
        <signal name="XLXN_63" />
        <signal name="Clk_50MHz" />
        <port polarity="Input" name="ROT_A" />
        <port polarity="Input" name="ROT_B" />
        <port polarity="Output" name="busy_led" />
        <port polarity="BiDirectional" name="OW_DQ" />
        <port polarity="Output" name="output_led" />
        <port polarity="Input" name="XLXN_61" />
        <port polarity="Input" name="Clk_50MHz" />
        <blockdef name="one_wire_bit">
            <timestamp>2016-5-6T9:44:41</timestamp>
            <line x2="384" y1="224" y2="224" x1="320" />
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="0" y1="-352" y2="-352" x1="64" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-352" y2="-352" x1="320" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-384" height="640" />
        </blockdef>
        <blockdef name="iobuf">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="128" y1="-128" y2="-128" x1="224" />
            <line x2="128" y1="-64" y2="-64" x1="160" />
            <line x2="160" y1="-128" y2="-64" x1="160" />
            <line x2="64" y1="-64" y2="-64" x1="0" />
            <line x2="96" y1="-140" y2="-192" x1="96" />
            <line x2="96" y1="-192" y2="-192" x1="0" />
            <line x2="64" y1="-96" y2="-160" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="128" />
            <line x2="128" y1="-160" y2="-128" x1="64" />
            <line x2="128" y1="-96" y2="-32" x1="128" />
            <line x2="128" y1="-64" y2="-96" x1="64" />
            <line x2="64" y1="-32" y2="-64" x1="128" />
            <line x2="64" y1="-128" y2="-128" x1="0" />
        </blockdef>
        <blockdef name="RotaryEnc">
            <timestamp>2016-4-22T10:0:8</timestamp>
            <rect width="256" x="64" y="-256" height="192" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <line x2="384" y1="-160" y2="-160" x1="320" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="gnd">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-64" y2="-96" x1="64" />
            <line x2="52" y1="-48" y2="-48" x1="76" />
            <line x2="60" y1="-32" y2="-32" x1="68" />
            <line x2="40" y1="-64" y2="-64" x1="88" />
            <line x2="64" y1="-64" y2="-80" x1="64" />
            <line x2="64" y1="-128" y2="-96" x1="64" />
        </blockdef>
        <blockdef name="inv">
            <timestamp>2000-1-1T10:10:10</timestamp>
            <line x2="64" y1="-32" y2="-32" x1="0" />
            <line x2="160" y1="-32" y2="-32" x1="224" />
            <line x2="128" y1="-64" y2="-32" x1="64" />
            <line x2="64" y1="-32" y2="0" x1="128" />
            <line x2="64" y1="0" y2="-64" x1="64" />
            <circle r="16" cx="144" cy="-32" />
        </blockdef>
        <blockdef name="one_wire_write_byte">
            <timestamp>2016-5-15T21:3:13</timestamp>
            <line x2="0" y1="160" y2="160" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="384" y1="96" y2="96" x1="320" />
            <line x2="0" y1="-288" y2="-288" x1="64" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <rect width="64" x="0" y="-44" height="24" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-288" y2="-288" x1="320" />
            <rect width="256" x="64" y="-320" height="512" />
        </blockdef>
        <blockdef name="one_wire_read_byte">
            <timestamp>2016-5-16T21:12:25</timestamp>
            <line x2="0" y1="32" y2="32" x1="64" />
            <line x2="384" y1="32" y2="32" x1="320" />
            <line x2="0" y1="-224" y2="-224" x1="64" />
            <line x2="0" y1="-160" y2="-160" x1="64" />
            <line x2="0" y1="-96" y2="-96" x1="64" />
            <line x2="0" y1="-32" y2="-32" x1="64" />
            <line x2="384" y1="-224" y2="-224" x1="320" />
            <rect width="64" x="320" y="-44" height="24" />
            <line x2="384" y1="-32" y2="-32" x1="320" />
            <rect width="256" x="64" y="-256" height="320" />
        </blockdef>
        <block symbolname="one_wire_bit" name="XLXI_1">
            <blockpin signalname="XLXN_33" name="wr1" />
            <blockpin signalname="XLXN_43" name="wr0" />
            <blockpin signalname="XLXN_61" name="rd" />
            <blockpin signalname="XLXN_63" name="rst" />
            <blockpin signalname="Clk_50MHz" name="clk" />
            <blockpin signalname="XLXN_44" name="busIn" />
            <blockpin signalname="busy_led" name="busy" />
            <blockpin signalname="XLXN_60" name="bit_output" />
            <blockpin signalname="XLXN_20" name="busOut" />
            <blockpin signalname="output_led" name="presence_indicator" />
        </block>
        <block symbolname="RotaryEnc" name="XLXI_3">
            <blockpin signalname="ROT_A" name="ROT_A" />
            <blockpin signalname="ROT_B" name="ROT_B" />
            <blockpin name="RotL" />
            <blockpin signalname="XLXN_63" name="RotR" />
            <blockpin signalname="Clk_50MHz" name="Clk" />
        </block>
        <block symbolname="gnd" name="XLXI_12">
            <blockpin signalname="XLXN_25" name="G" />
        </block>
        <block symbolname="iobuf" name="XLXI_2">
            <blockpin signalname="XLXN_25" name="I" />
            <blockpin signalname="OW_DQ" name="IO" />
            <blockpin signalname="XLXN_44" name="O" />
            <blockpin signalname="XLXN_20" name="T" />
        </block>
        <block symbolname="inv" name="XLXI_14">
            <blockpin signalname="busy_led" name="I" />
            <blockpin signalname="XLXN_62" name="O" />
        </block>
        <block symbolname="one_wire_write_byte" name="XLXI_18">
            <blockpin name="rst" />
            <blockpin signalname="Clk_50MHz" name="clk" />
            <blockpin signalname="XLXN_62" name="bitWriterEnabled" />
            <blockpin name="DI(7:0)" />
            <blockpin name="busy" />
            <blockpin signalname="XLXN_43" name="output0" />
            <blockpin signalname="XLXN_33" name="output1" />
            <blockpin name="enableWriting" />
        </block>
        <block symbolname="one_wire_read_byte" name="XLXI_19">
            <blockpin name="rst" />
            <blockpin signalname="Clk_50MHz" name="clk" />
            <blockpin signalname="XLXN_62" name="bitReaderEnabled" />
            <blockpin name="enabledReading" />
            <blockpin signalname="XLXN_60" name="rd_input" />
            <blockpin name="busy" />
            <blockpin signalname="XLXN_61" name="rd_output" />
            <blockpin name="DI(0:7)" />
        </block>
    </netlist>
    <sheet sheetnum="1" width="3520" height="2720">
        <branch name="ROT_A">
            <wire x2="752" y1="1200" y2="1200" x1="736" />
        </branch>
        <branch name="ROT_B">
            <wire x2="752" y1="1264" y2="1264" x1="736" />
        </branch>
        <instance x="752" y="1424" name="XLXI_3" orien="R0">
        </instance>
        <iomarker fontsize="28" x="736" y="1200" name="ROT_A" orien="R180" />
        <iomarker fontsize="28" x="736" y="1264" name="ROT_B" orien="R180" />
        <instance x="2128" y="1488" name="XLXI_2" orien="R0" />
        <instance x="1920" y="1488" name="XLXI_12" orien="R0" />
        <branch name="XLXN_25">
            <wire x2="2128" y1="1360" y2="1360" x1="1984" />
        </branch>
        <branch name="busy_led">
            <wire x2="1552" y1="704" y2="704" x1="1536" />
            <wire x2="1536" y1="704" y2="800" x1="1536" />
            <wire x2="1760" y1="800" y2="800" x1="1536" />
            <wire x2="1760" y1="800" y2="1072" x1="1760" />
            <wire x2="1856" y1="1072" y2="1072" x1="1760" />
            <wire x2="1728" y1="1056" y2="1056" x1="1696" />
            <wire x2="1728" y1="1056" y2="1072" x1="1728" />
            <wire x2="1760" y1="1072" y2="1072" x1="1728" />
        </branch>
        <branch name="OW_DQ">
            <wire x2="2384" y1="1360" y2="1360" x1="2352" />
        </branch>
        <iomarker fontsize="28" x="2384" y="1360" name="OW_DQ" orien="R0" />
        <iomarker fontsize="28" x="1856" y="1072" name="busy_led" orien="R0" />
        <instance x="1552" y="736" name="XLXI_14" orien="R0" />
        <instance x="1312" y="1408" name="XLXI_1" orien="R0">
        </instance>
        <branch name="XLXN_33">
            <wire x2="1296" y1="944" y2="1056" x1="1296" />
            <wire x2="1312" y1="1056" y2="1056" x1="1296" />
            <wire x2="2480" y1="944" y2="944" x1="1296" />
            <wire x2="2480" y1="720" y2="720" x1="2400" />
            <wire x2="2480" y1="720" y2="944" x1="2480" />
        </branch>
        <branch name="XLXN_44">
            <wire x2="1312" y1="1376" y2="1376" x1="1264" />
            <wire x2="1264" y1="1376" y2="1712" x1="1264" />
            <wire x2="2112" y1="1712" y2="1712" x1="1264" />
            <wire x2="2128" y1="1424" y2="1424" x1="2112" />
            <wire x2="2112" y1="1424" y2="1712" x1="2112" />
        </branch>
        <branch name="XLXN_20">
            <wire x2="1904" y1="1376" y2="1376" x1="1696" />
            <wire x2="1904" y1="1296" y2="1376" x1="1904" />
            <wire x2="2128" y1="1296" y2="1296" x1="1904" />
        </branch>
        <iomarker fontsize="28" x="528" y="1584" name="Clk_50MHz" orien="R180" />
        <branch name="XLXN_61">
            <wire x2="1216" y1="368" y2="368" x1="720" />
            <wire x2="1216" y1="368" y2="1184" x1="1216" />
            <wire x2="1312" y1="1184" y2="1184" x1="1216" />
        </branch>
        <instance x="2016" y="624" name="XLXI_18" orien="R0">
        </instance>
        <branch name="XLXN_63">
            <wire x2="1216" y1="1264" y2="1264" x1="1136" />
            <wire x2="1216" y1="1248" y2="1264" x1="1216" />
            <wire x2="1312" y1="1248" y2="1248" x1="1216" />
        </branch>
        <branch name="XLXN_43">
            <wire x2="1312" y1="1120" y2="1120" x1="1296" />
            <wire x2="1296" y1="1120" y2="1680" x1="1296" />
            <wire x2="2608" y1="1680" y2="1680" x1="1296" />
            <wire x2="2608" y1="656" y2="656" x1="2400" />
            <wire x2="2608" y1="656" y2="1680" x1="2608" />
        </branch>
        <branch name="XLXN_60">
            <wire x2="224" y1="368" y2="848" x1="224" />
            <wire x2="2688" y1="848" y2="848" x1="224" />
            <wire x2="2688" y1="848" y2="1184" x1="2688" />
            <wire x2="336" y1="368" y2="368" x1="224" />
            <wire x2="2688" y1="1184" y2="1184" x1="1696" />
            <wire x2="1696" y1="1184" y2="1504" x1="1696" />
        </branch>
        <branch name="XLXN_62">
            <wire x2="112" y1="240" y2="624" x1="112" />
            <wire x2="1792" y1="624" y2="624" x1="112" />
            <wire x2="1792" y1="624" y2="704" x1="1792" />
            <wire x2="336" y1="240" y2="240" x1="112" />
            <wire x2="1792" y1="704" y2="704" x1="1776" />
            <wire x2="2016" y1="464" y2="464" x1="1792" />
            <wire x2="1792" y1="464" y2="624" x1="1792" />
        </branch>
        <branch name="Clk_50MHz">
            <wire x2="272" y1="176" y2="560" x1="272" />
            <wire x2="512" y1="560" y2="560" x1="272" />
            <wire x2="512" y1="560" y2="1520" x1="512" />
            <wire x2="784" y1="1520" y2="1520" x1="512" />
            <wire x2="784" y1="1520" y2="1584" x1="784" />
            <wire x2="1264" y1="560" y2="560" x1="512" />
            <wire x2="336" y1="176" y2="176" x1="272" />
            <wire x2="784" y1="1584" y2="1584" x1="528" />
            <wire x2="752" y1="1328" y2="1328" x1="688" />
            <wire x2="688" y1="1328" y2="1504" x1="688" />
            <wire x2="784" y1="1504" y2="1504" x1="688" />
            <wire x2="784" y1="1504" y2="1520" x1="784" />
            <wire x2="1216" y1="1504" y2="1504" x1="784" />
            <wire x2="1216" y1="1312" y2="1504" x1="1216" />
            <wire x2="1312" y1="1312" y2="1312" x1="1216" />
            <wire x2="1264" y1="400" y2="560" x1="1264" />
            <wire x2="2016" y1="400" y2="400" x1="1264" />
        </branch>
        <instance x="336" y="336" name="XLXI_19" orien="R0">
        </instance>
        <iomarker fontsize="28" x="1840" y="1584" name="output_led" orien="R0" />
        <branch name="output_led">
            <wire x2="1760" y1="1632" y2="1632" x1="1696" />
            <wire x2="1840" y1="1584" y2="1584" x1="1760" />
            <wire x2="1760" y1="1584" y2="1632" x1="1760" />
        </branch>
    </sheet>
</drawing>