--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:35:43 05/06/2016
-- Design Name:   
-- Module Name:   C:/Users/lab/Desktop/1wire2/robsdrops-one-wire-6e71ad337537/tbw_WrByte.vhd
-- Project Name:  OneWire
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: one_wire_write_byte
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tbw_WrByte IS
END tbw_WrByte;
 
ARCHITECTURE behavior OF tbw_WrByte IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT one_wire_write_byte
    PORT(
         DI : IN  std_logic_vector(0 to 7);
         rst : IN  std_logic;
         clk : IN  std_logic;
         bitWriterEnabled : IN  std_logic;
         enableWriting : IN  std_logic;
         busy : OUT  std_logic;
         output0 : OUT  std_logic;
         output1 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal DI : std_logic_vector(0 to 7) := (others => '0');
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal bitWriterEnabled : std_logic := '1';
   signal enableWriting : std_logic := '0';

 	--Outputs
   signal busy : std_logic;
   signal output0 : std_logic;
   signal output1 : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: one_wire_write_byte PORT MAP (
          DI => DI,
          rst => rst,
          clk => clk,
          bitWriterEnabled => bitWriterEnabled,
          enableWriting => enableWriting,
          busy => busy,
          output0 => output0,
          output1 => output1
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 150 ns;	
		
		rst <= '1', '0' after clk_period;
		DI <= X"53";
		enableWriting <= '1', '0' after clk_period * 2;

      wait;
   end process;


   process
   begin
		loop
			wait until rising_edge( clk ) and ( output0 = '1' or output1 = '1' );
			bitWriterEnabled <= '0';
			wait for clk_period * 2;
			bitWriterEnabled <= '1';
		end loop;
   end process;
END;
