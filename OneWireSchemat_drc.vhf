--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : OneWireSchemat_drc.vhf
-- /___/   /\     Timestamp : 05/15/2016 23:02:05
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: /media/robert/3C95A4CA1E5C9738/Xilinx/14.7/ISE_DS/ISE/bin/lin64/unwrapped/sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl OneWireSchemat_drc.vhf -w "/home/robert/Documents/University of Technology/one-wire/OneWireSchemat.sch"
--Design Name: OneWireSchemat
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OneWireSchemat is
   port ( Clk_50MHz  : in    std_logic; 
          ROT_A      : in    std_logic; 
          ROT_B      : in    std_logic; 
          busy_led   : out   std_logic; 
          output_led : out   std_logic; 
          OW_DQ      : inout std_logic);
end OneWireSchemat;

architecture BEHAVIORAL of OneWireSchemat is
   attribute IOSTANDARD       : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   signal XLXN_20                           : std_logic;
   signal XLXN_25                           : std_logic;
   signal XLXN_33                           : std_logic;
   signal XLXN_43                           : std_logic;
   signal XLXN_44                           : std_logic;
   signal XLXN_56                           : std_logic;
   signal XLXN_60                           : std_logic;
   signal XLXN_61                           : std_logic;
   signal XLXN_62                           : std_logic;
   signal busy_led_DUMMY                    : std_logic;
   signal XLXI_15_DI_openSignal             : std_logic_vector (0 to 7);
   signal XLXI_15_enabledWriting_openSignal : std_logic;
   signal XLXI_17_enabledReading_openSignal : std_logic;
   component one_wire_bit
      port ( wr1                : in    std_logic; 
             wr0                : in    std_logic; 
             rd                 : in    std_logic; 
             rst                : in    std_logic; 
             clk                : in    std_logic; 
             busIn              : in    std_logic; 
             busy               : out   std_logic; 
             bit_output         : out   std_logic; 
             busOut             : out   std_logic; 
             presence_indicator : out   std_logic);
   end component;
   
   component IOBUF
      port ( I  : in    std_logic; 
             IO : inout std_logic; 
             O  : out   std_logic; 
             T  : in    std_logic);
   end component;
   attribute IOSTANDARD of IOBUF : component is "DEFAULT";
   attribute SLEW of IOBUF : component is "SLOW";
   attribute DRIVE of IOBUF : component is "12";
   attribute IBUF_DELAY_VALUE of IOBUF : component is "0";
   attribute IFD_DELAY_VALUE of IOBUF : component is "AUTO";
   attribute BOX_TYPE of IOBUF : component is "BLACK_BOX";
   
   component RotaryEnc
      port ( ROT_A : in    std_logic; 
             ROT_B : in    std_logic; 
             RotL  : out   std_logic; 
             RotR  : out   std_logic; 
             Clk   : in    std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component one_wire_write_byte
      port ( rst              : in    std_logic; 
             clk              : in    std_logic; 
             bitWriterEnabled : in    std_logic; 
             enabledWriting   : in    std_logic; 
             DI               : in    std_logic_vector (0 to 7); 
             busy             : out   std_logic; 
             output0          : out   std_logic; 
             output1          : out   std_logic);
   end component;
   
   component one_wire_read_byte
      port ( rst              : in    std_logic; 
             clk              : in    std_logic; 
             bitReaderEnabled : in    std_logic; 
             enabledReading   : in    std_logic; 
             rd_input         : in    std_logic; 
             busy             : out   std_logic; 
             rd_output        : out   std_logic; 
             DI               : out   std_logic_vector (0 to 7));
   end component;
   
begin
   busy_led <= busy_led_DUMMY;
   XLXI_1 : one_wire_bit
      port map (busIn=>XLXN_44,
                clk=>Clk_50MHz,
                rd=>XLXN_61,
                rst=>XLXN_56,
                wr0=>XLXN_43,
                wr1=>XLXN_33,
                bit_output=>XLXN_60,
                busOut=>XLXN_20,
                busy=>busy_led_DUMMY,
                presence_indicator=>open);
   
   XLXI_2 : IOBUF
      port map (I=>XLXN_25,
                T=>XLXN_20,
                O=>XLXN_44,
                IO=>OW_DQ);
   
   XLXI_3 : RotaryEnc
      port map (Clk=>Clk_50MHz,
                ROT_A=>ROT_A,
                ROT_B=>ROT_B,
                RotL=>open,
                RotR=>XLXN_56);
   
   XLXI_12 : GND
      port map (G=>XLXN_25);
   
   XLXI_14 : INV
      port map (I=>busy_led_DUMMY,
                O=>XLXN_62);
   
   XLXI_15 : one_wire_write_byte
      port map (bitWriterEnabled=>XLXN_62,
                clk=>Clk_50MHz,
                DI(0 to 7)=>XLXI_15_DI_openSignal(0 to 7),
                enabledWriting=>XLXI_15_enabledWriting_openSignal,
                rst=>XLXN_56,
                busy=>open,
                output0=>XLXN_43,
                output1=>XLXN_33);
   
   XLXI_17 : one_wire_read_byte
      port map (bitReaderEnabled=>XLXN_62,
                clk=>Clk_50MHz,
                enabledReading=>XLXI_17_enabledReading_openSignal,
                rd_input=>XLXN_60,
                rst=>XLXN_56,
                busy=>open,
                DI=>open,
                rd_output=>XLXN_61);
   
end BEHAVIORAL;


