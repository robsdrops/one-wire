----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:08:28 05/06/2016 
-- Design Name: 
-- Module Name:    one_wire_read_byte - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity one_wire_read_byte is
    Port ( rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           bitReaderEnabled : in  STD_LOGIC;
           enabledReading : in  STD_LOGIC;
			  rd_bit_input : in STD_LOGIC;
           busy : out  STD_LOGIC;
           DI : out  STD_LOGIC_VECTOR (7 downto 0);
           rd_bit_command : out  STD_LOGIC);
end one_wire_read_byte;

architecture Behavioral of one_wire_read_byte is
	type state_type is (IDLE, SET_READ_BIT_COMMAND, UNSET_READ_BIT_COMMAND,
							  RECEIVE_BIT, WAIT_FOR_READER);
	signal state : state_type := IDLE;
	signal next_state : state_type := IDLE;
	signal enabledReadingDummy : STD_LOGIC;
	signal read_counter : integer := 0;
	signal DIDummy : STD_LOGIC_VECTOR(7 downto 0) := "00000000";
begin

	main_process : process(clk)
	begin
		if rising_edge(clk) then
			state <= next_state;
		end if;
	end process;
	
	output_state_process : process(clk, state)
	begin
		if rising_edge(clk) then
			case state is
					when IDLE =>
						busy <= '0';
						DI <= DIDummy;
						enabledReadingDummy <= enabledReading;
						rd_bit_command <= '0';
						read_counter <= 0;
					when WAIT_FOR_READER =>
						busy <= '1';
					when SET_READ_BIT_COMMAND =>
						rd_bit_command <= '1';
					when UNSET_READ_BIT_COMMAND =>
						rd_bit_command <= '0';
					when RECEIVE_BIT =>
						DIDummy(read_counter) <= rd_bit_input;
						read_counter <= read_counter + 1;
			end case;
		end if;
	end process;	

	state_switcher : process(clk, state)
	begin
		next_state <= state;
		
		case state is
			when IDLE => 
				if enabledReadingDummy = '1' then
					next_state <= WAIT_FOR_READER;
				end if;
			when WAIT_FOR_READER =>
				if bitReaderEnabled = '1' then
					if read_counter = 8 then
						next_state <= IDLE;
					else
						next_state <=  SET_READ_BIT_COMMAND;
					end if;
				end if;
			when SET_READ_BIT_COMMAND =>
				next_state <= UNSET_READ_BIT_COMMAND;
			when UNSET_READ_BIT_COMMAND =>
			if bitReaderEnabled = '1' then
				next_state <= RECEIVE_BIT;
			end if;
			when RECEIVE_BIT =>
				next_state <= WAIT_FOR_READER;
		end case;
	end process;
end Behavioral;

