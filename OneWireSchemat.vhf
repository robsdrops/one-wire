--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____ 
--  /   /\/   / 
-- /___/  \  /    Vendor: Xilinx 
-- \   \   \/     Version : 14.7
--  \   \         Application : sch2hdl
--  /   /         Filename : OneWireSchemat.vhf
-- /___/   /\     Timestamp : 05/15/2016 23:08:39
-- \   \  /  \ 
--  \___\/\___\ 
--
--Command: sch2hdl -intstyle ise -family spartan3e -flat -suppress -vhdl "/home/robert/Documents/University of Technology/one-wire/OneWireSchemat.vhf" -w "/home/robert/Documents/University of Technology/one-wire/OneWireSchemat.sch"
--Design Name: OneWireSchemat
--Device: spartan3e
--Purpose:
--    This vhdl netlist is translated from an ECS schematic. It can be 
--    synthesized and simulated, but it should not be modified. 
--

library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity OneWireSchemat is
   port ( Clk_50MHz  : in    std_logic; 
          ROT_A      : in    std_logic; 
          ROT_B      : in    std_logic; 
          busy_led   : out   std_logic; 
          output_led : out   std_logic; 
          OW_DQ      : inout std_logic);
end OneWireSchemat;

architecture BEHAVIORAL of OneWireSchemat is
   attribute IOSTANDARD       : string ;
   attribute SLEW             : string ;
   attribute DRIVE            : string ;
   attribute IBUF_DELAY_VALUE : string ;
   attribute IFD_DELAY_VALUE  : string ;
   attribute BOX_TYPE         : string ;
   signal XLXN_20                          : std_logic;
   signal XLXN_25                          : std_logic;
   signal XLXN_33                          : std_logic;
   signal XLXN_43                          : std_logic;
   signal XLXN_44                          : std_logic;
   signal XLXN_56                          : std_logic;
   signal XLXN_60                          : std_logic;
   signal XLXN_61                          : std_logic;
   signal XLXN_62                          : std_logic;
   signal busy_led_DUMMY                   : std_logic;
   signal XLXI_18_DI_openSignal            : std_logic_vector (7 downto 0);
   signal XLXI_18_enableWriting_openSignal : std_logic;
   component one_wire_bit
      port ( wr1                : in    std_logic; 
             wr0                : in    std_logic; 
             rd                 : in    std_logic; 
             rst                : in    std_logic; 
             clk                : in    std_logic; 
             busIn              : in    std_logic; 
             busy               : out   std_logic; 
             bit_output         : out   std_logic; 
             busOut             : out   std_logic; 
             presence_indicator : out   std_logic);
   end component;
   
   component IOBUF
      port ( I  : in    std_logic; 
             IO : inout std_logic; 
             O  : out   std_logic; 
             T  : in    std_logic);
   end component;
   attribute IOSTANDARD of IOBUF : component is "DEFAULT";
   attribute SLEW of IOBUF : component is "SLOW";
   attribute DRIVE of IOBUF : component is "12";
   attribute IBUF_DELAY_VALUE of IOBUF : component is "0";
   attribute IFD_DELAY_VALUE of IOBUF : component is "AUTO";
   attribute BOX_TYPE of IOBUF : component is "BLACK_BOX";
   
   component RotaryEnc
      port ( ROT_A : in    std_logic; 
             ROT_B : in    std_logic; 
             RotL  : out   std_logic; 
             RotR  : out   std_logic; 
             Clk   : in    std_logic);
   end component;
   
   component GND
      port ( G : out   std_logic);
   end component;
   attribute BOX_TYPE of GND : component is "BLACK_BOX";
   
   component INV
      port ( I : in    std_logic; 
             O : out   std_logic);
   end component;
   attribute BOX_TYPE of INV : component is "BLACK_BOX";
   
   component one_wire_write_byte
      port ( rst              : in    std_logic; 
             clk              : in    std_logic; 
             bitWriterEnabled : in    std_logic; 
             DI               : in    std_logic_vector (7 downto 0); 
             busy             : out   std_logic; 
             output0          : out   std_logic; 
             output1          : out   std_logic; 
             enableWriting    : in    std_logic);
   end component;
   
begin
   busy_led <= busy_led_DUMMY;
   XLXI_1 : one_wire_bit
      port map (busIn=>XLXN_44,
                clk=>Clk_50MHz,
                rd=>XLXN_61,
                rst=>XLXN_56,
                wr0=>XLXN_43,
                wr1=>XLXN_33,
                bit_output=>XLXN_60,
                busOut=>XLXN_20,
                busy=>busy_led_DUMMY,
                presence_indicator=>open);
   
   XLXI_2 : IOBUF
      port map (I=>XLXN_25,
                T=>XLXN_20,
                O=>XLXN_44,
                IO=>OW_DQ);
   
   XLXI_3 : RotaryEnc
      port map (Clk=>Clk_50MHz,
                ROT_A=>ROT_A,
                ROT_B=>ROT_B,
                RotL=>open,
                RotR=>XLXN_56);
   
   XLXI_12 : GND
      port map (G=>XLXN_25);
   
   XLXI_14 : INV
      port map (I=>busy_led_DUMMY,
                O=>XLXN_62);
   
   XLXI_18 : one_wire_write_byte
      port map (bitWriterEnabled=>XLXN_62,
                clk=>Clk_50MHz,
                DI(7 downto 0)=>XLXI_18_DI_openSignal(7 downto 0),
                enableWriting=>XLXI_18_enableWriting_openSignal,
                rst=>XLXN_56,
                busy=>open,
                output0=>XLXN_43,
                output1=>XLXN_33);
   
end BEHAVIORAL;


