--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:33:52 04/22/2016
-- Design Name:   
-- Module Name:   C:/Users/lab/Desktop/ONE_WIRE/robsdrops-one-wire-cf2d34790817/wr1_tb.vhd
-- Project Name:  OneWire
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: one_wire_bit
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY wr1_tb IS
END wr1_tb;
 
ARCHITECTURE behavior OF wr1_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT one_wire_bit
    PORT(
         wr1 : IN  std_logic;
         wr0 : IN  std_logic;
         rd : IN  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
         busy : OUT  std_logic;
         indicator : OUT  std_logic;
         busOut : OUT  std_logic;
         busIn : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal wr1 : std_logic := '0';
   signal wr0 : std_logic := '0';
   signal rd : std_logic := '0';
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal busIn : std_logic := '0';

 	--indicators
   signal busy : std_logic;
   signal indicator : std_logic;
   signal busOut : std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: one_wire_bit PORT MAP (
          wr1 => wr1,
          wr0 => wr0,
          rd => rd,
          rst => rst,
          clk => clk,
          busy => busy,
          indicator => indicator,
          busOut => busOut,
          busIn => busIn
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

		rst <= '1', '0' after 30 ns;
		wait for 540 us;
		busIn <= '0';
		wait for 500 us;
		wr1 <= '1', '0' after 30 ns;
		wait for 100 us;

      wait;
   end process;

END;
